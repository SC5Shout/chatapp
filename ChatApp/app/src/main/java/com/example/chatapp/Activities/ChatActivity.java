package com.example.chatapp.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.chatapp.Adapters.MessagesAdapter;
import com.example.chatapp.Config.Config;
import com.example.chatapp.Config.InboxConfig;
import com.example.chatapp.Config.MessageConfig;
import com.example.chatapp.Models.ChatMessage;
import com.example.chatapp.Models.ChatMessageType;
import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.R;
import com.example.chatapp.RequestManager;
import com.example.chatapp.SocketManager;
import com.example.chatapp.Utils.BitmapHelper;
import com.example.chatapp.Utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import lombok.SneakyThrows;

public class ChatActivity extends AppCompatActivity {
    private ChatUser user;
    private ChatUser receiverUser;

    private EditText messageInput;
    private ImageButton sendButton;
    private RecyclerView messagesView;
    private ProgressBar messagesProgress;

    private MessagesAdapter messagesAdapter;
    private final List<ChatMessage> messages = Collections.synchronizedList(new ArrayList<>());
    private final Map<Long, Integer> messagesIdTable = Collections.synchronizedMap(new HashMap<>());

    private final CompositeDisposable messageQueue = new CompositeDisposable();

    private ActivityResultLauncher<Intent> cameraResult;

    @SneakyThrows
    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        PreferenceManager preferenceManager = new PreferenceManager(this);

        String loggedUserString = preferenceManager.getString(Config.KEY_USER);
        user = new ChatUser(new JSONObject(loggedUserString));

        String receiverUserString = getIntent().getStringExtra(Config.KEY_RECEIVER);
        receiverUser = new ChatUser(new JSONObject(receiverUserString));

        TextView name = findViewById(R.id.name);
        name.setText(receiverUser.getName());

        messagesProgress = findViewById(R.id.messagesProgess);

        messagesView = findViewById(R.id.messagesView);
        messagesAdapter = new MessagesAdapter(messages, user);
        messagesView.setAdapter(messagesAdapter);

        ImageButton backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(view -> BackButtonAction());

        messageInput = findViewById(R.id.messageInput);
        messageInput.requestFocus();
        sendButton = findViewById(R.id.sendButton);

        LoadMessagesFromDatabase();
        UpdateInboxSeen(true);
        UpdateAllMessagesReceived(true);

        sendButton.setOnClickListener(view -> SendMessage(null));

        cameraResult = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            Bitmap photo = (Bitmap)data.getExtras().get("data");
                            SendMessage(photo);
                        }
                    }
                });

        ImageButton cameraButton = findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(view -> {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraResult.launch(cameraIntent);
        });

        ReceiveMessage();
        UpdateMessageSeen();
    }

    @SuppressLint("NotifyDataSetChanged")
    private void LoadMessagesFromDatabase() {
        Loading(true);
        JsonArrayRequest messageRequest = new JsonArrayRequest(Request.Method.GET,
                MessageConfig.getMessageBySenderAndReceiverUrl(user.getId(), receiverUser.getId()),
                null,
                jsonMessages -> {
                    Loading(false);
                    if(messages.size() == jsonMessages.length()) {
                        return;
                    }

                    try {
                        int count = messages.size();
                        for (int i = count; i < jsonMessages.length(); i++) {
                            JSONObject jresponse = jsonMessages.getJSONObject(i);
                            ChatMessage msg = new ChatMessage(jresponse);
                            msg.setSentByService(true);
                            messages.add(msg);
                            messagesIdTable.put(msg.getId(), messages.size() - 1);
                        }

                        if(count == 0) {
                            messagesAdapter.notifyDataSetChanged();
                        } else {
                            messagesAdapter.notifyItemRangeInserted(messages.size(), messages.size());
                            messagesView.smoothScrollToPosition(messages.size() - 1);
                        }
                    } catch (JSONException e) {
                        OnRequestError(e);
                    }
                },
                this::OnRequestError
        );

        RequestManager.getInstance(this).AddToRequestQueue(messageRequest);
    }

    private void Loading(Boolean isLoading) {
        if(isLoading) {
            messagesView.setVisibility(View.GONE);
            messagesProgress.setVisibility(View.VISIBLE);
            messageInput.setClickable(false);
            sendButton.setClickable(false);
        } else {
            messagesView.setVisibility(View.VISIBLE);
            messagesProgress.setVisibility(View.GONE);
            messageInput.setClickable(true);
            sendButton.setClickable(true);
        }
    }

    @SneakyThrows
    @SuppressLint("NotifyDataSetChanged")
    private void SendMessage(Bitmap image) {
        String messageText = messageInput.getText().toString();
        if(messageText.isEmpty() && image == null) {
            return;
        }

        ChatMessageType messageType = ChatMessageType.TEXT;

        if(image != null) {
            messageText = Base64.encodeToString(BitmapHelper.getBytes(image), Base64.DEFAULT);
            messageType = ChatMessageType.PNG;
        }

        //first, create a new message with invalid ID, because it's given by the service, but right now id does not matter
        //add it into the message list
        //and then:
        //1. if the request returns some actual value, update this message id and other data
        //2. if the request returns error notify the adapter that the message is not sent.
        ChatMessage msg = new ChatMessage(0L, user, receiverUser, messageText, messageType, true, false, false, false);
        {
            int count = messages.size();
            messages.add(msg);
            if(count == 0) {
                messagesAdapter.notifyDataSetChanged();
            } else {
                messagesAdapter.notifyItemRangeInserted(messages.size(), messages.size());
                messagesView.smoothScrollToPosition(messages.size() - 1);
            }
        }

        JSONObject obj = new JSONObject();
        obj.put("senderId", user.getId());
        obj.put("receiverId", receiverUser.getId());
        obj.put("message", messageText);
        obj.put("messageType", messageType.getValue());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, MessageConfig.SEND_MESSAGE_URL, obj, response -> {
            try {
                ChatMessage msgFromRequest = new ChatMessage(response);

                //- 1 because the message list has already at least 1 message
                int count = messages.size() - 1;
                //TODO: investigate
                //this may create a bug when the user sends messages too fast.
                // ---- I checked it out and it sometimes doesn't update the message "seen" variable appropriately, it doesn't affect UX, though.
                // ---- But I don't think it's the only problem
                // ---- Also, I'm not sure if the user will be able to send messages THAT fast, probably the bigger problem is the slow service
                //
                //or when the service is too slow to process all tasks
                //
                //one thing that can fix this (I think) is to first, remove the msg from the messages
                //and add it again. This way the count variable will be up to date
                //
                //I don't know if the volley executes the queue on 1 thread (but different than the main) or it uses few threads which complicate the situation
                //or if it does internally some magic and the queue is not executed in order the requests are queued.
                //may be worth reading about
                ChatMessage message = messages.get(count);
                message.Clone(msgFromRequest).setSentByService(true);
                messagesIdTable.put(message.getId(), count);
                messagesAdapter.notifyDataSetChanged();

                messageInput.setText("");

            } catch (JSONException e) {
                msg.setError(true);
                messagesAdapter.notifyDataSetChanged();
                e.printStackTrace();
            }
        }, throwable -> {
            msg.setError(true);
            messagesAdapter.notifyDataSetChanged();
            throwable.printStackTrace();
        });

        RequestManager.getInstance(this).AddToRequestQueue(request);
    }

    @SuppressLint({"CheckResult", "NotifyDataSetChanged"})
    private void ReceiveMessage() {
        messageQueue.add(SocketManager.getInstance().ReceivePeriodic(MessageConfig.getReceiveMessageTopic(user.getId(), receiverUser.getId()), topicMessage -> {
            String msgString = topicMessage.getPayload();

            int count = messages.size();
            ChatMessage msg = new ChatMessage(new JSONObject(msgString));
            messages.add(msg);
            messagesIdTable.put(msg.getId(), messages.size() - 1);

            if(count == 0) {
                messagesAdapter.notifyDataSetChanged();
            } else {
                messagesAdapter.notifyItemRangeInserted(messages.size(), messages.size());
                messagesView.smoothScrollToPosition(messages.size() - 1);
            }

            UpdateInboxSeen(true);
            UpdateMessageReceived(msg.getId(), true);
        }, this::OnRequestError));
    }

    @SuppressLint("NotifyDataSetChanged")
    private void UpdateMessageSeen() {
        //The receiver sends a request that the message is received, where senderId is receiverId because receiverId is actually id from where the message comes
        //other words, on the user side, the receiver is the sender of the message when dealing with back-requests
        messageQueue.add(SocketManager.getInstance().ReceivePeriodic(MessageConfig.getMessageSeenTopic(receiverUser.getId()), topicMessage -> {
            String msgString = topicMessage.getPayload();

            JSONObject obj = new JSONObject(msgString);
            long messageId = obj.getLong("messageId");
            boolean seen = obj.getBoolean("seen");

            Integer index = messagesIdTable.get(messageId);
            if(index != null) {
                ChatMessage msg = messages.get(index);
                msg.setSeen(seen);
                messagesAdapter.notifyDataSetChanged();
            }

        }, this::OnRequestError));
    }

    private void UpdateInboxSeen(boolean value) {
        JsonObjectRequest jsonRequest
                = new JsonObjectRequest(Request.Method.PATCH, InboxConfig.UpdateInboxSeenUrl(user.getId(), receiverUser.getId(), value), null,
            response -> {},
                Throwable::printStackTrace);

        RequestManager.getInstance(this).AddToRequestQueue(jsonRequest);
    }

    private void UpdateMessageReceived(Long messageId, boolean value) {
        JsonObjectRequest jsonRequest
                = new JsonObjectRequest(Request.Method.PATCH, MessageConfig.UpdateMessageSeenUrl(user.getId(), messageId, value), null,
            response -> {},
            this::OnRequestError);

        RequestManager.getInstance(this).AddToRequestQueue(jsonRequest);
    }

    private void UpdateAllMessagesReceived(boolean value) {
        JsonObjectRequest jsonRequest
                = new JsonObjectRequest(Request.Method.PATCH, MessageConfig.UpdateAllMessagesSeenUrl(user.getId(), receiverUser.getId(), value), null,
            response -> {},
            this::OnRequestError);

        RequestManager.getInstance(this).AddToRequestQueue(jsonRequest);
    }

    void BackButtonAction() {
        if (messageQueue != null) {
            messageQueue.dispose();
        }

        Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    void OnRequestError(Throwable throwable) {
        throwable.printStackTrace();
        if (messageQueue != null) {
            messageQueue.dispose();
        }

        Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
        startActivity(intent);
        finish();
    }
}