package com.example.chatapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.chatapp.Adapters.ActiveUsersAdapter;
import com.example.chatapp.Adapters.InboxAdapter;
import com.example.chatapp.Config.Config;
import com.example.chatapp.Config.InboxConfig;
import com.example.chatapp.Config.UserConfig;
import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.Models.Inbox;
import com.example.chatapp.R;
import com.example.chatapp.RequestManager;
import com.example.chatapp.SocketManager;
import com.example.chatapp.Utils.CacheManager;
import com.example.chatapp.Utils.PreferenceManager;
import com.example.chatapp.Utils.UserCallback;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.disposables.CompositeDisposable;
import lombok.SneakyThrows;

public class MainMenuActivity extends AppCompatActivity implements UserCallback {
    static private final String TAG = "TOPIC_QUEUE";

    private final static CacheManager<ChatUser> activeUsers = new CacheManager<>();
    private final static CacheManager<Inbox> inboxes = new CacheManager<>();

    private ChatUser loggedUser;
    private final InboxAdapter inboxAdapter = new InboxAdapter(inboxes.getData(), this);
    private final ActiveUsersAdapter activeUsersAdapter = new ActiveUsersAdapter(activeUsers.getData(), this);

    private TextView noActiveUsersText;
    private ProgressBar activeUsersProgressBar;

    private TextView noInboxesText;
    private ProgressBar inboxesProgressBar;

    private PreferenceManager preferenceManager;
    private CompositeDisposable topicsQueue;

    @SneakyThrows
    @RequiresApi(api = Build.VERSION_CODES.S)
    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        preferenceManager = new PreferenceManager(this);
        String loggedUserString = preferenceManager.getString(Config.KEY_USER);
        loggedUser = new ChatUser(new JSONObject(loggedUserString));

        RecyclerView activeUsersView = findViewById(R.id.activeUsers);
        activeUsersView.setAdapter(activeUsersAdapter);

        RecyclerView inboxView = findViewById(R.id.inbox);
        inboxView.setAdapter(inboxAdapter);

        ImageButton logoutButton = findViewById(R.id.button);
        logoutButton.setOnClickListener(view -> {
            Logout();
        });

        TextView name = findViewById(R.id.name);
        name.setText(loggedUser.getName());

        noActiveUsersText = findViewById(R.id.noActiveUsersText);
        activeUsersProgressBar = findViewById(R.id.activeUsersProgressBar);

        noInboxesText = findViewById(R.id.noInboxesText);
        inboxesProgressBar = findViewById(R.id.inboxesProgressBar);

        if(!preferenceManager.getBoolean(Config.KEY_IS_SIGNED_IN)) {
            preferenceManager.putBoolean(Config.KEY_IS_SIGNED_IN, true);
            AddToActiveUsers();
        }

        SocketManager.getInstance().Connect();
        ResetSubscriptions();

        LoadingUsers(true);
        LoadActiveUsersFromDatabase();

        LoadingInboxes(true);
        LoadInboxesFromDatabase();

        UpdateActiveUsers();
        UpdateInboxes();
    }

    @SneakyThrows
    @Override
    public void onUserClicked(ChatUser receiver) {
        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
        intent.putExtra(Config.KEY_RECEIVER, receiver.getJson().toString());

        startActivity(intent);
        finish();
    }

    void Logout() {
        ResetSubscriptions();
        SocketManager.getInstance().Disconnect();
        RemoveFromActiveUsers();
        preferenceManager.clear();
        activeUsers.clear();
        inboxes.clear();
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        ResetSubscriptions();
        super.onDestroy();
    }

    void OnRequestError(Throwable throwable) {
        throwable.printStackTrace();
        if(preferenceManager.getBoolean(Config.KEY_IS_SIGNED_IN)) {
            Toast.makeText(getApplicationContext(), "Could not connect to the service, try again later", Toast.LENGTH_SHORT).show();
            Logout();
        }
    }

    void ResetSubscriptions() {
        if (topicsQueue != null) {
            topicsQueue.dispose();
        }
        topicsQueue = new CompositeDisposable();
    }

    private void AddToActiveUsers() {
        try {
            JSONObject user = loggedUser.getJson();
            JSONObject obj = new JSONObject();
            obj.put(Config.KEY_USER, user);

            JsonObjectRequest jsonRequest
                    = new JsonObjectRequest(Request.Method.POST,
                        UserConfig.ADD_ACTIVE_USER_URL,
                        obj,
                        response -> {},
                        this::OnRequestError);

            RequestManager.getInstance(this).AddToRequestQueue(jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void RemoveFromActiveUsers() {
        StringRequest request = new StringRequest(Request.Method.DELETE, UserConfig.getRemoveActiveUserByIdURL(loggedUser.getId()),
                response -> {},
                this::OnRequestError
        );

        RequestManager.getInstance(this).AddToRequestQueue(request);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void LoadActiveUsersFromDatabase() {
        JsonArrayRequest usersRequest = new JsonArrayRequest(Request.Method.GET, UserConfig.getActiveUsersExceptUrl(loggedUser.getId()), null,
                jsonResult -> {
                    try {
                        int count = activeUsers.size();
                        for (int i = count; i < jsonResult.length(); i++) {
                            JSONObject jresponse = jsonResult.getJSONObject(i).getJSONObject(Config.KEY_USER);
                            ChatUser user = new ChatUser(jresponse);
                            activeUsers.Add(user);
                        }

                        if(count == 0) {
                            activeUsersAdapter.notifyDataSetChanged();
                        } else activeUsersAdapter.notifyItemRangeInserted(activeUsers.size(), activeUsers.size());

                        LoadingUsers(false);
                    } catch (JSONException e) {
                        LoadingUsers(false);
                        e.printStackTrace();
                    }
                },
                this::OnRequestError
        );

        RequestManager.getInstance(this).AddToRequestQueue(usersRequest);
    }

    @SuppressLint({"CheckResult", "NotifyDataSetChanged"})
    private void UpdateActiveUsers() {
        topicsQueue.add(SocketManager.getInstance().ReceivePeriodic(UserConfig.ADD_ACTIVE_USER_TOPIC, topicMessage-> {
            Log.d(TAG, "UpdateActiveUsers: add");
            String userMsg = topicMessage.getPayload();
            ChatUser user = new ChatUser(new JSONObject(userMsg));
            if(user.getId().equals(loggedUser.getId())) {
                return;
            }
            int count = activeUsers.size();
            activeUsers.Add(user);

            if(count == 0) {
                activeUsersAdapter.notifyDataSetChanged();
            } else activeUsersAdapter.notifyItemRangeInserted(activeUsers.size(), activeUsers.size());

            LoadingUsers(false);
        }, this::OnRequestError));

        topicsQueue.add(SocketManager.getInstance().ReceivePeriodic(UserConfig.REMOVE_ACTIVE_USER_TOPIC, topicMessage-> {
            Log.d(TAG, "UpdateActiveUsers: remove");
            String userMsg = topicMessage.getPayload();
            long id = Long.parseLong(userMsg);
            if(id == loggedUser.getId()) {
                return;
            }
            int count = activeUsers.size();

            if(count == 0) {
                LoadingUsers(false);
            } else {
                int index = activeUsers.RemoveAndGetIndex(id);
                activeUsersAdapter.notifyItemRemoved(index);
                //activeUsersAdapter.notifyDataSetChanged();
                count = activeUsers.size();
                if(count == 0) {
                    LoadingUsers(false);
                }
            }
        }, this::OnRequestError));
    }

    private void LoadingUsers(boolean isLoading) {
        if(isLoading) {
            activeUsersProgressBar.setVisibility(View.VISIBLE);
            noActiveUsersText.setVisibility(View.GONE);
        } else {
            activeUsersProgressBar.setVisibility(View.GONE);
            if(activeUsers.isEmpty()) {
                noActiveUsersText.setVisibility(View.VISIBLE);
            } else noActiveUsersText.setVisibility(View.GONE);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void LoadInboxesFromDatabase() {
        JsonArrayRequest messageRequest = new JsonArrayRequest(Request.Method.GET, InboxConfig.getInboxesOfUrl(loggedUser.getId()), null,
                jsonInboxes -> {
                    try {
                        int count = inboxes.size();
                        for (int i = count; i < jsonInboxes.length(); i++) {
                            JSONObject jresponse = jsonInboxes.getJSONObject(i);
                            Inbox inbox = new Inbox(jresponse);
                            inboxes.Add(inbox);
                        }

                        if(count == 0) {
                            inboxAdapter.notifyDataSetChanged();
                        } else inboxAdapter.notifyItemRangeInserted(inboxes.size(), inboxes.size());

                        LoadingInboxes(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this::OnRequestError
        );

        RequestManager.getInstance(this).AddToRequestQueue(messageRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    @SuppressLint({"CheckResult", "NotifyDataSetChanged"})
    private void UpdateInboxes() {
        topicsQueue.add(SocketManager.getInstance().ReceivePeriodic(InboxConfig.getInboxesTopic(loggedUser.getId()), topicMessage -> {
            Log.d(TAG, "UpdateInboxes: update");
            String inboxMsg = topicMessage.getPayload();
            JSONObject jresponse = new JSONObject(inboxMsg);
            Inbox inbox = new Inbox(jresponse);

            inboxes.Add(inbox);
            inboxAdapter.notifyDataSetChanged();

            LoadingInboxes(false);
        }, this::OnRequestError));
    }

    private void LoadingInboxes(boolean isLoading) {
        if(isLoading) {
            inboxesProgressBar.setVisibility(View.VISIBLE);
            noInboxesText.setVisibility(View.GONE);
        } else {
            inboxesProgressBar.setVisibility(View.GONE);
            if(inboxes.isEmpty()) {
                noInboxesText.setVisibility(View.VISIBLE);
            } else noInboxesText.setVisibility(View.GONE);
        }
    }
}