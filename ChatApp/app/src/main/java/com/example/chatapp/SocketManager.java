package com.example.chatapp;

import com.example.chatapp.Config.Config;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompMessage;

public class SocketManager {
    private static SocketManager instance;
    private StompClient socket;
    private boolean connected = false;

    private SocketManager() {
        socket = getSocket();
    }

    public static synchronized SocketManager getInstance() {
        if (instance == null) {
            instance = new SocketManager();
        }
        return instance;
    }

    public StompClient getSocket() {
        if (socket == null) {
            socket = Stomp.over(Stomp.ConnectionProvider.OKHTTP, Config.SOCKET_URL);
        }
        return socket;
    }

    public void Connect() {
        if(!connected) {
            socket.connect();
            connected = true;
        }
    }

    public void Disconnect() {
        if(connected) {
            socket.disconnect();
            connected = false;
        }
    }

    public Completable Send(String destination, String data) {
        return socket.send(destination, data);
    }

    public Flowable<StompMessage> Receive(String destinationPath) {
        return socket.topic(destinationPath);
    }

    public Disposable ReceivePeriodic(String destinationPath, Consumer<? super StompMessage> onNext, Consumer<? super Throwable> onError) {
        return socket.topic(destinationPath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError);
    }
}
