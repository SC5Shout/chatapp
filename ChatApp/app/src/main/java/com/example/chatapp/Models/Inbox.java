package com.example.chatapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Inbox implements ObjectWithId {
    private Long id;
    private ChatUser senderUser;
    private ChatUser receiverUser;
    private String lastMessage;
    private ChatMessageType messageType;
    private boolean seen;
    private boolean you;

    public Inbox(ChatUser senderUser, ChatUser receiverUser, String lastMessage, ChatMessageType messageType, boolean seen) {
        this.senderUser = senderUser;
        this.receiverUser = receiverUser;
        this.lastMessage = lastMessage;
        this.messageType = messageType;
        this.seen = seen;
    }

    public Inbox(JSONObject json) throws JSONException {
        id = json.getLong("id");
        senderUser = new ChatUser(json.getJSONObject("sender"));
        receiverUser = new ChatUser(json.getJSONObject("receiver"));
        lastMessage = json.getString("lastMessage");
        messageType = ChatMessageType.valueOf(json.getString("messageType"));
        seen = json.getBoolean("seen");
        you = json.getBoolean("you");
    }

    public JSONObject getJson() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("sender", senderUser.getJson());
        object.put("receiver", receiverUser.getJson());
        object.put("lastMessage", lastMessage);
        object.put("messageType", messageType.getValue());
        object.put("seen", seen);
        object.put("you", you);
        return object;
    }

    @Override
    public Long getId() {
        return id;
    }
}