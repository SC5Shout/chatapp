package com.example.chatapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.chatapp.Config.Config;
import com.example.chatapp.Config.UserConfig;
import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.Activities.MainMenuActivity;
import com.example.chatapp.RequestManager;
import com.example.chatapp.R;
import com.example.chatapp.Utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SignupTabFragment extends Fragment {
    private EditText nameText;
    private EditText emailText;
    private EditText passwordText;
    private EditText rePasswordText;
    private Button signupButton;
    private ProgressBar signupProgress;
    private PreferenceManager preferenceManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_signup_tab, container, false);

        preferenceManager = new PreferenceManager(requireContext());

        nameText = root.findViewById(R.id.name);
        emailText = root.findViewById(R.id.email);

        passwordText = root.findViewById(R.id.password);
        rePasswordText = root.findViewById(R.id.re_password);

        signupProgress = root.findViewById(R.id.signupProgress);

        signupButton = root.findViewById(R.id.signup_button);
        signupButton.setOnClickListener(view -> {
            Loading(true);

            if(ValidateUserData()) {
                Register();
            } else Loading(false);
        });

        return root;
    }

    private boolean ValidateUserData() {
        nameText.setBackgroundResource(R.drawable.edit_text_shape);
        emailText.setBackgroundResource(R.drawable.edit_text_shape);
        passwordText.setBackgroundResource(R.drawable.edit_text_shape);
        rePasswordText.setBackgroundResource(R.drawable.edit_text_shape);

        String name = nameText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String repassword = rePasswordText.getText().toString();

        if(name.equals("")) {
            nameText.setError("Enter your name");
            nameText.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
            nameText.requestFocus();
            return false;
        }

        if(name.equals("NULL")) {
            nameText.setError("Invalid name");
            nameText.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
            nameText.requestFocus();
            return false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Invalid email address");
            emailText.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
            emailText.requestFocus();
            return false;
        }

        if(password.equals("") || !password.matches(Config.PASSWORD_REGEX)) {
            passwordText.setError("The password has to be minimum eight characters, at least one letter and one number");
            passwordText.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
            passwordText.requestFocus();
            return false;
        }

        if(!repassword.equals(password) || !repassword.matches(Config.PASSWORD_REGEX)) {
            rePasswordText.setError("Passwords don't match");
            rePasswordText.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
            rePasswordText.requestFocus();
            return false;
        }

        return true;
    }

    private void AlreadyRegistered(EditText resource) {
        resource.setError(resource.getText().toString() + " is already taken");
        resource.setBackgroundResource(com.example.chatapp.R.drawable.edit_text_shape_invalid_input);
    }

    private void Register() {
        HashMap<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("name", nameText.getText().toString());
        params.put("email", emailText.getText().toString());
        params.put("password", passwordText.getText().toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, UserConfig.NEW_USER_URL, new JSONObject(params),
                response -> {
                    try {
                        boolean justCreated = response.getBoolean("justCreated");

                        if(justCreated) {
                            ChatUser user = new ChatUser(response);
                            preferenceManager.putString(Config.KEY_USER, user.getJson().toString());

                            Intent intent = new Intent(getContext(), MainMenuActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            AlreadyRegistered(nameText);
                            AlreadyRegistered(emailText);
                            Loading(false);
                        }
                    } catch (JSONException e) {
                        Loading(false);
                        e.printStackTrace();
                    }
                }, error ->{
                    Loading(false);
                    error.printStackTrace();
                }
        );

        RequestManager.getInstance(getContext()).AddToRequestQueue(jsonRequest);
    }

    private void Loading(boolean isLoading) {
        if(isLoading) {
            signupProgress.setVisibility(View.VISIBLE);
            nameText.setVisibility(View.GONE);
            emailText.setVisibility(View.GONE);
            passwordText.setVisibility(View.GONE);
            rePasswordText.setVisibility(View.GONE);
            signupButton.setVisibility(View.GONE);
        } else {
            signupProgress.setVisibility(View.GONE);
            nameText.setVisibility(View.VISIBLE);
            emailText.setVisibility(View.VISIBLE);
            passwordText.setVisibility(View.VISIBLE);
            rePasswordText.setVisibility(View.VISIBLE);
            signupButton.setVisibility(View.VISIBLE);
        }
    }
}