package com.example.chatapp.Models;

public interface ObjectWithId {
    Long getId();
}
