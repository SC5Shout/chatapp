package com.example.chatapp.Models;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class LoginResult {
    private LoginState state;
}
