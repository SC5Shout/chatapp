package com.example.chatapp.Adapters;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Models.ChatMessageType;
import com.example.chatapp.Models.Inbox;
import com.example.chatapp.R;
import com.example.chatapp.Utils.UserCallback;
import com.google.android.material.card.MaterialCardView;

import java.util.List;
import java.util.Objects;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {
    private final List<Inbox> inboxes;
    private final UserCallback onUserClick;

    public InboxAdapter(List<Inbox> inboxes, UserCallback onUserClick) {
        this.inboxes = inboxes;
        this.onUserClick = onUserClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setData(Objects.requireNonNull(inboxes.get(position)), onUserClick);
    }

    @Override
    public int getItemCount() {
        return inboxes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView name;
        TextView message;
        TextView you;

        MaterialCardView cv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            avatar = itemView.findViewById(R.id.avatar);
            name = itemView.findViewById(R.id.name);
            message = itemView.findViewById(R.id.message);
            you = itemView.findViewById(R.id.you);
            cv = itemView.findViewById(R.id.cardView);
        }

        private void ChangeTextIForgotTheName(TextView resource, float size, int color, int typeface) {
            resource.setTextSize(size);
            resource.setTextColor(color);
            resource.setTypeface(null, typeface);
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n"})
        public void setData(Inbox inbox, UserCallback onUserClick) {
            cv.setOnClickListener(view -> {
                onUserClick.onUserClicked(inbox.getReceiverUser());
            });
            name.setText(inbox.getReceiverUser().getName());

            String msg = inbox.getLastMessage();
            if(inbox.getMessageType() == ChatMessageType.PNG) {
                msg = "sent a photo";
            }
            message.setText(msg);

            if(inbox.isSeen()) {
                ChangeTextIForgotTheName(message, 20, ContextCompat.getColor(itemView.getContext(), R.color.grey), Typeface.NORMAL);
                ChangeTextIForgotTheName(you, 20, ContextCompat.getColor(itemView.getContext(), R.color.grey), Typeface.NORMAL);
            } else {
                ChangeTextIForgotTheName(message, 22, ContextCompat.getColor(itemView.getContext(), R.color.black), Typeface.BOLD);
                ChangeTextIForgotTheName(you, 22, ContextCompat.getColor(itemView.getContext(), R.color.black), Typeface.BOLD);
            }

            if(inbox.isYou()) {
                you.setVisibility(View.VISIBLE);
            } else you.setVisibility(View.GONE);
        }
    }
}
