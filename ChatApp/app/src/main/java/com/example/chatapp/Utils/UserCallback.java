package com.example.chatapp.Utils;

import com.example.chatapp.Models.ChatUser;

public interface UserCallback {
    void onUserClicked(ChatUser result);
}
