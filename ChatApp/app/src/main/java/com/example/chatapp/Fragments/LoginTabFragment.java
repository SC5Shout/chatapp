package com.example.chatapp.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.chatapp.Config.Config;
import com.example.chatapp.Config.UserConfig;
import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.Models.LoginResult;
import com.example.chatapp.Activities.MainMenuActivity;
import com.example.chatapp.Models.LoginState;
import com.example.chatapp.RequestManager;
import com.example.chatapp.R;
import com.example.chatapp.Utils.PreferenceManager;

import org.json.JSONException;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor
public class LoginTabFragment extends Fragment {
    private EditText emailEdit;
    private EditText passwordEdit;
    private ProgressBar loginProgress;
    private Button loginButton;
    private PreferenceManager preferenceManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login_tab, container, false);

        preferenceManager = new PreferenceManager(requireContext());

        if(preferenceManager.getBoolean(Config.KEY_IS_SIGNED_IN)) {
            Intent intent = new Intent(getContext(), MainMenuActivity.class);
            startActivity(intent);
            requireActivity().finish();
        }

        emailEdit = root.findViewById(R.id.email);
        passwordEdit = root.findViewById(R.id.password);
        loginProgress = root.findViewById(R.id.loginProgress);

        loginButton = root.findViewById(R.id.login);
        loginButton.setOnClickListener(view -> {
            Login();
        });

        return root;
    }

    private void Login() {
        Loading(true);
        emailEdit.setBackgroundResource(R.drawable.edit_text_shape);
        passwordEdit.setBackgroundResource(R.drawable.edit_text_shape);

        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();

        boolean emailMatches = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        boolean passwordMatches = !password.equals("") && password.matches(Config.PASSWORD_REGEX);

        if(!emailMatches || !passwordMatches) {
            InvalidData();
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, UserConfig.getLoginUrl(email, password), null, jsonResult-> {
            try {
                String jsonRes = jsonResult.getString("state");
                LoginResult loginResult = new LoginResult(jsonRes.equals("SUCCEED") ? LoginState.SUCCEED : LoginState.FAILED);
                if(loginResult.getState() == LoginState.SUCCEED) {
                    ChatUser loggedUser = new ChatUser(jsonResult.getJSONObject("user"));
                    PerformLoginTask(loggedUser);
                } else InvalidData();
            } catch (JSONException e) {
                InvalidData();
                e.printStackTrace();
            }
        }, error ->{
            InvalidData();
            error.printStackTrace();
        });

        RequestManager.getInstance(requireContext().getApplicationContext()).AddToRequestQueue(jsonObjectRequest);
    }

    @SneakyThrows
    private void PerformLoginTask(ChatUser user) {
        preferenceManager.putString(Config.KEY_USER, user.getJson().toString());

        Intent intent = new Intent(getContext(), MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void InvalidData() {
        Loading(false);

        emailEdit.setError("Invalid email");
        emailEdit.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);

        passwordEdit.setError("Invalid password");
        passwordEdit.setBackgroundResource(R.drawable.edit_text_shape_invalid_input);
    }

    private void Loading(boolean isLoading) {
        if(isLoading) {
            loginProgress.setVisibility(View.VISIBLE);
            emailEdit.setVisibility(View.GONE);
            passwordEdit.setVisibility(View.GONE);
            loginButton.setVisibility(View.GONE);
        } else {
            loginProgress.setVisibility(View.GONE);
            emailEdit.setVisibility(View.VISIBLE);
            passwordEdit.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.VISIBLE);
        }
    }
}