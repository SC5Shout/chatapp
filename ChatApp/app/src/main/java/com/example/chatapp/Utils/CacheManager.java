package com.example.chatapp.Utils;

import android.os.Build;

import com.example.chatapp.Models.ObjectWithId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CacheManager<T extends ObjectWithId> {
    private List<T> data;
    private Map<Long, Integer> idToIndex;
    private Map<Integer, Long> indexToId;

    public CacheManager() {
        data = Collections.synchronizedList(new ArrayList<>());
        idToIndex = Collections.synchronizedMap(new HashMap<>());
        indexToId = Collections.synchronizedMap(new HashMap<>());
    }

    public CacheManager(CacheManager<T> other) {
        data = Collections.synchronizedList(new ArrayList<>(other.data));
        idToIndex = Collections.synchronizedMap(new HashMap<>(other.idToIndex));
        indexToId = Collections.synchronizedMap(new HashMap<>(other.indexToId));
    }

    public T Add(T value) {
        Integer index = idToIndex.get(value.getId());
        if(index == null) {
            int newIndex = data.size();
            data.add(value);
            idToIndex.put(value.getId(), newIndex);
            indexToId.put(newIndex, value.getId());
            return data.get(newIndex);
        } else {
            data.set(index, value);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                idToIndex.replace(value.getId(), index);
                indexToId.replace(index, value.getId());
            } else {
                idToIndex.put(value.getId(), index);
                indexToId.put(index, value.getId());
            }
            return data.get(index);
        }
    }

    public void Remove(Long id) {
        Integer index = idToIndex.get(id);
        if(index == null) {
            throw new RuntimeException("CacheManager::Remove(): Object under the id: " + id + " does not exists");
        } else {
            data.remove(index.intValue());
            idToIndex.remove(id);
        }
    }

    public int RemoveAndGetIndex(Long id) {
        Integer index = idToIndex.get(id);
        if(index == null) {
            throw new RuntimeException("CacheManager::Remove(): Object under the id: " + id + " does not exists");
        } else {
            data.remove(index.intValue());
            idToIndex.remove(id);
            indexToId.remove(index);

            //because when the item is removed from data, the indexes shifts down by one
            //eg. id -> index
            //when we don't shift the indexToId map
            // 1 -> 0
            // 2 -> 1
            // 3 -> 2
            // id 2 is removed
            // 1 -> 0
            // 3 -> 2 ----> ERROR!!! index 2 is invalid, because the ArrayList shifted the object under id 3 down by one
            //To fix this, the idToIndex map's value also has to be shifted down by one
            ShiftIdToIndex(index);

            return index;
        }
    }

    public T get(Long id) {
        Integer index = idToIndex.get(id);
        if(index == null) {
            throw new RuntimeException("CacheManager::get(): Object under the id: " + id + " does not exists");
        } else return data.get(index);
    }

    public int size() {
        return data.size();
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public void clear() {
        data.clear();
        idToIndex.clear();
    }

    public void AddAll(CacheManager<T> other) {
        data.addAll(other.data);
        idToIndex.putAll(other.idToIndex);
    }

    private void ShiftIdToIndex(int index) {
        for(int i = index; i <= idToIndex.size(); ++i) {
            Long oldId = indexToId.get(i);
            if(oldId != null) {
                Integer oldIndex = idToIndex.get(oldId);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    idToIndex.replace(oldId, Objects.requireNonNull(oldIndex) - 1);
                } else {
                    idToIndex.put(oldId, oldIndex);
                }
            }
        }
    }
}
