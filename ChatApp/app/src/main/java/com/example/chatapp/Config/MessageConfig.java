package com.example.chatapp.Config;

public class MessageConfig {
    static public final String SEND_MESSAGE_URL = Config.URL + "/message/send/";
    static public final String MESSAGE_URL = Config.URL + "/message";
    static public String getMessageBySenderAndReceiverUrl(Long senderId, Long receiverId) { return MESSAGE_URL + "/bysenderandreceiver/" + senderId + "/" + receiverId; }
    static public String UpdateMessageSeenUrl(Long senderId, Long messageId, boolean value) { return MESSAGE_URL + "/seen/" + senderId + "/" + messageId + "/" + value; }
    static public String UpdateAllMessagesSeenUrl(Long senderId, Long receiverId, boolean value) { return MESSAGE_URL + "/all/seen/" + senderId + "/" + receiverId + "/" + value; }

    static public String getReceiveMessageTopic(Long receiverId, Long senderId) { return "/queue/receiver" + receiverId + "/" + senderId; }
    static public String getMessageSeenTopic(Long senderId) { return "/queue/message.received/" + senderId; }
}
