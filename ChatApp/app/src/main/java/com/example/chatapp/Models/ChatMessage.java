package com.example.chatapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ChatMessage implements ObjectWithId {
    private Long id;
    private ChatUser senderUser;
    private ChatUser receiverUser;
    private String message;
    private ChatMessageType messageType;
    private boolean sentToService;
    private boolean sentByService;
    private boolean seen;
    private boolean error;

    public ChatMessage(JSONObject json) throws JSONException {
        id = json.getLong("id");
        senderUser = new ChatUser(json.getJSONObject("sender"));
        receiverUser = new ChatUser(json.getJSONObject("receiver"));
        message = json.getString("message");
        messageType = ChatMessageType.valueOf(json.getString("messageType"));
        seen = json.getBoolean("seen");
    }

    public ChatMessage Clone(ChatMessage other) {
        id = other.id;
        senderUser = other.senderUser;
        receiverUser = other.receiverUser;
        message = other.message;
        messageType = other.messageType;
        sentToService = other.sentToService;
        sentByService = other.sentByService;
        seen = other.seen;
        return this;
    }

    public JSONObject getJson() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("sender", senderUser.getJson());
        object.put("receiver", receiverUser.getJson());
        object.put("message", message);
        object.put("messageType", messageType);
        object.put("seen", seen);

        return object;
    }

    @Override
    public Long getId() {
        return id;
    }
}
