package com.example.chatapp.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.chatapp.Config.Config;

public class PreferenceManager {
    final private SharedPreferences sharedPreferences;

    public PreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(Config.PREFERENCE_MANAGER_NAME, Context.MODE_PRIVATE);
    }
    
    public void putBoolean(String name, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    public boolean getBoolean(String name) {
        return sharedPreferences.getBoolean(name, false);
    }

    public void putString(String name, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public String getString(String name) {
        return sharedPreferences.getString(name, null);
    }

    public void RemoveString(String name) {
        sharedPreferences.edit().remove(name).apply();
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }
}
