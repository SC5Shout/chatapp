package com.example.chatapp.Adapters;

import android.graphics.Bitmap;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Config.Config;
import com.example.chatapp.Models.ChatMessage;
import com.example.chatapp.Models.ChatMessageType;
import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.R;
import com.example.chatapp.Utils.BitmapHelper;

import java.util.List;
import java.util.Objects;

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<ChatMessage> messages;
    private final ChatUser senderUser;

    static final private int VIEW_TYPE_SENDER = 1;
    static final private int VIEW_TYPE_RECEIVER = 2;

    public MessagesAdapter(List<ChatMessage> messages, ChatUser senderUser) {
        this.messages = messages;
        this.senderUser = senderUser;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_SENDER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);
            return new SenderViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_message_item, parent, false);
            return new ReceiverViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == VIEW_TYPE_SENDER) {
            ((SenderViewHolder)holder).setData(Objects.requireNonNull(messages.get(position)));
        } else ((ReceiverViewHolder)holder).setData(Objects.requireNonNull(messages.get(position)));
    }

    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(Objects.requireNonNull(messages.get(position)).getSenderUser().getId().equals(senderUser.getId())) {
            return VIEW_TYPE_SENDER;
        } return VIEW_TYPE_RECEIVER;
    }

    interface ConstraintsCallback {
        void change();
    }

    static void setImageMessageData(String textMessage, ImageView messageImage, TextView messageText, ConstraintsCallback constraintsCallback) {
        byte[] decodedString = Base64.decode(textMessage, Base64.DEFAULT);
        Bitmap bitmap = BitmapHelper.getImage(decodedString);

        messageImage.setImageBitmap(bitmap);
        messageImage.setVisibility(View.VISIBLE);
        messageText.setVisibility(View.INVISIBLE);

        constraintsCallback.change();
    }

    static void setTextMessageData(String textMessage, ImageView messageImage, TextView messageText, ConstraintsCallback constraintsCallback) {
        messageImage.setVisibility(View.INVISIBLE);
        messageText.setVisibility(View.VISIBLE);
        messageText.setText(textMessage);

        constraintsCallback.change();
    }

    static class SenderViewHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        ImageView messageImage;
        ImageView sentIcon;

        public SenderViewHolder(@NonNull View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.textMessage);
            messageImage = itemView.findViewById(R.id.imageMessage);
            sentIcon = itemView.findViewById(R.id.sentIcon);
        }

        private void setData(ChatMessage message) {
            if(message.isSentToService()) {
                sentIcon.setImageResource(R.drawable.sent_icon);
            } else if(message.isSentByService()) {
                sentIcon.setImageResource(R.drawable.received_icon);
            }

            if(message.isSeen()) {
                sentIcon.setImageResource(R.drawable.seen_icon);
            } else if(message.isError()) {
                sentIcon.setImageResource(R.drawable.error_icon);
            }
            sentIcon.setVisibility(View.VISIBLE);

            String textMessage = message.getMessage();
            if(message.getMessageType() == ChatMessageType.PNG) {
                setImageMessageData(textMessage, messageImage, messageText, () -> {
                    ConstraintLayout constraintLayout = itemView.findViewById(R.id.messageLayout);
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(constraintLayout);
                    constraintSet.connect(R.id.sentIcon, ConstraintSet.TOP, R.id.imageMessage, ConstraintSet.TOP,0);
                    constraintSet.connect(R.id.sentIcon, ConstraintSet.BOTTOM, R.id.imageMessage, ConstraintSet.BOTTOM,0);
                    constraintSet.applyTo(constraintLayout);
                });
            } else {
                setTextMessageData(textMessage, messageImage, messageText, () -> {
                    ConstraintLayout constraintLayout = itemView.findViewById(R.id.messageLayout);
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(constraintLayout);
                    constraintSet.connect(R.id.sentIcon, ConstraintSet.TOP, R.id.textMessage, ConstraintSet.TOP,0);
                    constraintSet.connect(R.id.sentIcon, ConstraintSet.BOTTOM, R.id.textMessage, ConstraintSet.BOTTOM,0);
                    constraintSet.applyTo(constraintLayout);
                });
            }
        }
    }

    static class ReceiverViewHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        ImageView messageImage;

        public ReceiverViewHolder(@NonNull View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.textMessage);
            messageImage = itemView.findViewById(R.id.imageMessage);
        }

        private void setData(ChatMessage message) {
            String textMessage = message.getMessage();
            if(message.getMessageType() == ChatMessageType.PNG) {
                setImageMessageData(textMessage, messageImage, messageText, () -> {/*nothing*/});
            } else {
                setTextMessageData(textMessage, messageImage, messageText, () -> {/*nothing*/});
            }
        }
    }
}
