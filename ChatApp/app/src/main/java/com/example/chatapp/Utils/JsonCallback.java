package com.example.chatapp.Utils;

import org.json.JSONObject;

public interface JsonCallback {
    void onSuccess(JSONObject result);
}
