package com.example.chatapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Models.ChatUser;
import com.example.chatapp.R;
import com.example.chatapp.Utils.UserCallback;

import java.util.List;

public class ActiveUsersAdapter extends RecyclerView.Adapter<ActiveUsersAdapter.ViewHolder> {
    private final List<ChatUser> activeUsers;
    private final UserCallback onUserClick;

    public ActiveUsersAdapter(List<ChatUser> activeUsers, UserCallback onUserClick) {
        this.activeUsers = activeUsers;
        this.onUserClick = onUserClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_user_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatUser user = activeUsers.get(position);
        if(user != null) {
            holder.cv.setOnClickListener(view -> {
                onUserClick.onUserClicked(user);
            });

            holder.nameView.setText(user.getName());
        }
    }

    @Override
    public int getItemCount() {
        return activeUsers == null ? 0 : activeUsers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameView;
        ImageView avatarImage;
        CardView cv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cv = itemView.findViewById(R.id.cardView);
            nameView = itemView.findViewById(R.id.name);
            avatarImage = itemView.findViewById(R.id.avatar);
        }
    }
}
