package com.example.chatapp.Config;

public class UserConfig {
    static public final String ACTIVE_USER_URL = Config.URL + "/activeuser";
    static public final String ADD_ACTIVE_USER_URL = ACTIVE_USER_URL + "/add";
    static public String getActiveUsersExceptUrl(long id) { return ACTIVE_USER_URL + "/allexcept/" + id; }
    static public String getRemoveActiveUserByIdURL(long id) { return ACTIVE_USER_URL + "/remove" + "/" + id; }

    static public final String ADD_ACTIVE_USER_TOPIC = "/topic/activeuser/add";
    static public final String REMOVE_ACTIVE_USER_TOPIC = "/topic/activeuser/remove";

    static public final String USER_URL = Config.URL + "/user";
    static public final String NEW_USER_URL = USER_URL + "/new";
    static public String getLoginUrl(String email, String password) { return USER_URL + "/login/" + email + "/" + password; }
}
