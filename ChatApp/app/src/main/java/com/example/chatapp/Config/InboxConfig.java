package com.example.chatapp.Config;

public class InboxConfig {
    static public final String INBOX_URL = Config.URL + "/inbox";
    static public String getInboxesOfUrl(Long senderId) { return INBOX_URL + "/allof/" + senderId; }
    static public String UpdateInboxSeenUrl(Long senderId, Long receiverId, boolean value) { return INBOX_URL + "/seen/" + senderId + "/" + receiverId + "/" + value; }

    static public String getInboxesTopic(Long senderId) { return "/topic/inboxes/" + senderId; }
}
