package com.example.chatapp.Config;

public class Config {
    static public final String PREFERENCE_MANAGER_NAME = "ChatAppReference";
    static public final String KEY_IS_SIGNED_IN = "isSignedIn";

    static public final String URL = "http://10.0.2.2:8080";
    static public final String SOCKET_URL = "ws://10.0.2.2:8080/chat/websocket";

    static public final String KEY_USER = "user";
    static public final String KEY_RECEIVER = "receiver";

    static public final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$";
}
