package com.example.chatapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ChatUser implements ObjectWithId {
    private Long id;
    private String email;
    private String name;
    public ChatUser(JSONObject json) throws JSONException {
        id = json.getLong("id");
        name = json.getString("name");
        email = json.getString("email");
    }

    public JSONObject getJson() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("id", id);
        object.put("name", name);
        object.put("email", email);

        return object;
    }

    @Override
    public Long getId() {
        return id;
    }
}
