package com.example.chatservice.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto {
    private long id;
    private String name;
    private String email;

    //the frontend signup code uses this to identify if the user is already registered or not
    private Boolean justCreated = false;

    static public UserDto FromChatUser(ChatUser user) {
        UserDto result = new UserDto();
        result.id = user.getId();
        result.name = user.getName();
        result.email = user.getEmail();
        return result;
    }
}
