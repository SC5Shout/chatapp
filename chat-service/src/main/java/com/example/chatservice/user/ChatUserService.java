package com.example.chatservice.user;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChatUserService {
    private final ChatUserRepository repository;

    public ChatUserService(ChatUserRepository repository) {
        this.repository = repository;
    }

    public UserDto AddNewUser(ChatUser user) {
        Optional<ChatUser> userByEmail = repository.findChatUserByEmail(user.getEmail());
        if (userByEmail.isPresent()) {
            return userByEmail.map(UserDto::FromChatUser).get();
        }

        Optional<ChatUser> userByName = repository.findChatUserByName(user.getName());
        if (userByName.isPresent()) {
            return userByName.map(UserDto::FromChatUser).get();
        }

        ChatUser u = repository.save(user);

        var dto = UserDto.FromChatUser(u);
        dto.setJustCreated(true);
        return dto;
    }

    public LoginResult Login(String email, String password) {
        var optUser = repository.findChatUserByEmailAndPassword(email, password).map(UserDto::FromChatUser);
        return optUser
                .map(user -> new LoginResult(optUser.get(), LoginState.SUCCEED))
                .orElseGet(() -> new LoginResult(null, LoginState.FAILED));
    }

    public Optional<ChatUser> FindUserById(Long id) {
        return repository.findChatUserById(id);
    }
}
