package com.example.chatservice.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "user")
public class ChatUserController {
    private final ChatUserService userService;

    @Autowired
    ChatUserController(ChatUserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "new")
    public UserDto RegisterNewUser(@RequestBody ChatUser newUser) {
        return userService.AddNewUser(newUser);
    }

    @GetMapping(path = "login/{email}/{password}")
    public LoginResult Login(@PathVariable String email, @PathVariable String password) {
        return userService.Login(email, password);
    }
}