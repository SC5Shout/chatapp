package com.example.chatservice.user;

import lombok.*;

enum LoginState {
    FAILED,
    SUCCEED
}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginResult {
    private UserDto user;
    private LoginState state;
}
