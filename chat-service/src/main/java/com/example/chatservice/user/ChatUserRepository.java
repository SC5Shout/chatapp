package com.example.chatservice.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatUserRepository extends JpaRepository<ChatUser, Long> {
    Optional<ChatUser> findChatUserByEmail(String email);
    Optional<ChatUser> findChatUserByName(String name);
    Optional<ChatUser> findChatUserByEmailAndPassword(String email, String password);
    Optional<ChatUser> findChatUserById(Long id);
}
