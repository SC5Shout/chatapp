package com.example.chatservice.activeUser;

import com.example.chatservice.user.UserDto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ActiveUserDto {
    private long id;
    private UserDto user;

    static public ActiveUserDto FromActiveUser(ActiveUser user) {
        ActiveUserDto result = new ActiveUserDto();
        result.id = user.getId();
        result.user = UserDto.FromChatUser(user.getUser());
        return result;
    }
}
