package com.example.chatservice.activeUser;

import com.example.chatservice.user.UserDto;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ActiveUserMessageService {
    private SimpMessagingTemplate messagingTemplate;

    public void NotifyFrontendAdd(UserDto dto) {
        messagingTemplate.convertAndSend("/topic/activeuser/add", dto);
    }

    public void NotifyFrontendRemove(Long id) {
        messagingTemplate.convertAndSend("/topic/activeuser/remove", id);
    }
}
