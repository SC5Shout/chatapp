package com.example.chatservice.activeUser;

import com.example.chatservice.user.ChatUserService;
import com.example.chatservice.user.UserDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActiveUserService {
    private final ActiveUserRepository repository;
    private final ChatUserService userService;
    private final ActiveUserMessageService messageService;

    public ActiveUserService(ActiveUserRepository repository, ChatUserService userService, ActiveUserMessageService messageService) {
        this.repository = repository;
        this.userService = userService;
        this.messageService = messageService;
    }

    public ActiveUserDto Add(ActiveUserDto dto) {
        var userRef = userService.FindUserById(dto.getUser().getId()).orElseThrow(() -> {
            throw new IllegalStateException("Cannot add active user: " + dto + " because user is not registered yet");
        });

        var activeUserOpt = repository.findActiveUserByUser(userRef);

        var activeUser = activeUserOpt.orElseGet(() -> {
            ActiveUser newUser = new ActiveUser(userRef);
            var result = repository.save(newUser);
            messageService.NotifyFrontendAdd(UserDto.FromChatUser(userRef));
            return result;
        });
        return ActiveUserDto.FromActiveUser(activeUser);
    }

    public void Remove(long userId) {
        var userRef = userService.FindUserById(userId).orElseThrow(() -> {
            throw new IllegalStateException("The user id: " + userId + " is not registered yet");
        });

        var activeUser = repository.findActiveUserByUser(userRef).orElseThrow(() -> {
            throw new IllegalStateException("Cannot remove the user with id " + userId + ". The user is not active or already removed.");
        });

        repository.delete(activeUser);
        messageService.NotifyFrontendRemove(userId);
    }

    public List<ActiveUserDto> getActiveUsersExcept(long userId) {
        var user = userService.FindUserById(userId);
        return user.map(chatUser -> repository.getActiveUsersByUserIsNot(chatUser)
                .stream()
                .map(ActiveUserDto::FromActiveUser)
                .collect(Collectors.toList()))
                .orElseGet(ArrayList::new);
    }
}
