package com.example.chatservice.activeUser;

import com.example.chatservice.user.ChatUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActiveUserRepository extends JpaRepository<ActiveUser, Long> {
    List<ActiveUser> getActiveUsersByUserIsNot(ChatUser user);
    Optional<ActiveUser> findActiveUserByUser(ChatUser user);
}

