package com.example.chatservice.activeUser;

import com.example.chatservice.user.ChatUser;
import lombok.*;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import javax.persistence.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ActiveUser")
@Table(name = "ActiveUser")
public class ActiveUser {
    public ActiveUser(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getLong("id");
        this.user = new ChatUser(jsonObject.getJSONObject("userId"));
    }

    public ActiveUser(ChatUser user) {
        this.user = user;
    }

    @Id
    @SequenceGenerator(name = "active_user_sequence", sequenceName = "active_user_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "active_user_sequence")
    @Column(name = "id", updatable = false)
    private long id;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private ChatUser user;
}