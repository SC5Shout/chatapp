package com.example.chatservice.activeUser;

import com.example.chatservice.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(path = "activeuser")
public class ActiveUserController {
    private final ActiveUserService activeUserService;

    @Autowired
    ActiveUserController(ActiveUserService service) {
        this.activeUserService = service;
    }

    @PostMapping(path = "add")
    public UserDto Add(@RequestBody ActiveUserDto userDto) {
        return activeUserService.Add(userDto).getUser();
    }

    @DeleteMapping(path = "remove/{userId}")
    public void Remove(@PathVariable Long userId) {
        activeUserService.Remove(userId);
    }

    @GetMapping(path = "allexcept/{userId}")
    public List<ActiveUserDto> getUsersExcept(@PathVariable Long userId) {
        return activeUserService.getActiveUsersExcept(userId);
    }
}
