package com.example.chatservice.inbox;

import com.example.chatservice.user.ChatUser;
import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Inbox")
@Table(name = "Inbox")
public class Inbox {
    public Inbox(ChatUser sender, ChatUser receiver, String lastMessage, String messageType, boolean seen, boolean you) {
        this.sender = sender;
        this.receiver = receiver;
        this.lastMessage = lastMessage;
        this.seen = seen;
        this.you = you;
        this.messageType = messageType;
    }

    public Inbox Clone(Inbox other) {
        this.sender = other.getSender();
        this.receiver = other.getReceiver();;
        this.lastMessage = other.getLastMessage();
        this.seen = other.isSeen();
        this.you = other.isYou();
        this.messageType = other.messageType;

        return this;
    }

    @Id
    @SequenceGenerator(name = "char_inbox_sequence", sequenceName = "char_inbox_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "char_inbox_sequence")
    @Column(name = "id", updatable = false)
    private long id;

    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "senderId", referencedColumnName = "id")
    private ChatUser sender;

    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "receiverId", referencedColumnName = "id")
    private ChatUser receiver;

    @Column(name = "lastMessage", nullable = false, columnDefinition = "TEXT")
    private String lastMessage;

    @Column(name = "messageType", nullable = false, columnDefinition = "TEXT")
    private String messageType;

    @Column(name = "seen", nullable = false, columnDefinition = "BOOL")
    private boolean seen;

    @Column(name = "you", nullable = false, columnDefinition = "BOOL")
    private boolean you;
}