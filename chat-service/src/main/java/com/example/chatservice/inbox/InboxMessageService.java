package com.example.chatservice.inbox;

import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InboxMessageService {
    private final SimpMessagingTemplate messagingTemplate;

    public void NotifyFrontend(InboxDto dto) {
        messagingTemplate.convertAndSend("/topic/inboxes/" + dto.getSender().getId(), dto);
    }
}
