package com.example.chatservice.inbox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "inbox")
public class InboxController {
    private final InboxService inboxService;

    @Autowired
    public InboxController(InboxService inboxService) {
        this.inboxService = inboxService;
    }

    @GetMapping(path = "allof/{senderId}")
    public List<InboxDto> getInboxesOfSenderId(@PathVariable Long senderId) {
        return inboxService.getInboxesOfSender(senderId);
    }

    @PatchMapping(path = "seen/{senderId}/{receiverId}/{value}")
    public InboxDto UpdateSeen(@PathVariable Long senderId, @PathVariable Long receiverId, @PathVariable boolean value) throws IllegalStateException {
        return inboxService.UpdateSeen(senderId, receiverId, value);
    }
}