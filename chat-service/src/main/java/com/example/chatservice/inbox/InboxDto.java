package com.example.chatservice.inbox;

import com.example.chatservice.message.ChatMessageType;
import com.example.chatservice.user.UserDto;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InboxDto {
    private long id;
    private UserDto sender;
    private UserDto receiver;
    private String lastMessage;
    private ChatMessageType messageType;
    private boolean seen;
    private boolean you;

    static public InboxDto FromInbox(Inbox inbox) {
        InboxDto result = new InboxDto();

        result.id = inbox.getId();
        result.sender = UserDto.FromChatUser(inbox.getSender());
        result.receiver = UserDto.FromChatUser(inbox.getReceiver());
        result.lastMessage = inbox.getLastMessage();
        result.seen = inbox.isSeen();
        result.you = inbox.isYou();
        result.messageType = ChatMessageType.valueOf(inbox.getMessageType());

        return result;
    }
}
