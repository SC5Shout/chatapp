package com.example.chatservice.inbox;

import com.example.chatservice.user.ChatUserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InboxService {
    private final InboxRepository repository;
    private final InboxMessageService messageService;
    private final ChatUserService userService;

    public InboxService(InboxRepository repository, InboxMessageService messageService, ChatUserService userService) {
        this.repository = repository;
        this.messageService = messageService;
        this.userService = userService;
    }

    public void AddInbox(Inbox inbox) {
        Optional<Inbox> optInbox = repository.findInboxBySenderAndReceiver(inbox.getSender(), inbox.getReceiver());

        if (optInbox.isPresent()) {
            Inbox oldInbox = optInbox.get();
            UpdateInbox(oldInbox, inbox);
            return;
        }

        var result = InboxDto.FromInbox(repository.save(inbox));
        messageService.NotifyFrontend(result);
    }

    public List<InboxDto> getInboxesOfSender(long senderId) {
        var sender = userService.FindUserById(senderId);
        return sender.map(user -> repository
                .getInboxesBySender(user)
                .stream()
                .map(InboxDto::FromInbox)
                .collect(Collectors.toList())).orElseGet(ArrayList::new);
    }

    //TODO: investigate why @Transactional does not work
    //@Transactional
    void UpdateInbox(Inbox oldInbox, Inbox newInbox) {
        oldInbox.Clone(newInbox);

        repository.save(oldInbox);
        messageService.NotifyFrontend(InboxDto.FromInbox(oldInbox));
    }

    @Transactional
    public InboxDto UpdateSeen(Long senderId, Long receiverId, boolean value) {
        var sender = userService.FindUserById(senderId).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var receiver = userService.FindUserById(receiverId).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var inbox = repository.findInboxBySenderAndReceiver(sender, receiver).orElseThrow(() -> {
            throw new IllegalStateException("inbox does not exist");
        });

        inbox.setSeen(value);
        var result = InboxDto.FromInbox(inbox);
        messageService.NotifyFrontend(result);
        return result;
    }
}
