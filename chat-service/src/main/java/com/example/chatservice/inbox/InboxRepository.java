package com.example.chatservice.inbox;

import com.example.chatservice.user.ChatUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InboxRepository extends JpaRepository<Inbox, Long> {
    Optional<Inbox> findInboxBySenderAndReceiver(ChatUser sender, ChatUser receiver);
    List<Inbox> getInboxesBySender(ChatUser sender);
}
