package com.example.chatservice.message;

import com.example.chatservice.inbox.Inbox;
import com.example.chatservice.inbox.InboxService;
import com.example.chatservice.user.ChatUserService;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {
    private final MessageRepository repository;
    private final InboxService inboxService;
    private final ChatUserService userService;
    private final SendMessageService sendMessageService;

    public MessageService(MessageRepository repository,
                          InboxService inboxService,
                          ChatUserService userService,
                          SendMessageService sendMessageService) {
        this.repository = repository;
        this.inboxService = inboxService;
        this.userService = userService;
        this.sendMessageService = sendMessageService;
    }

    public ChatMessageDto AddMessage(SimpMessage simpMessage) throws IllegalStateException {
        var sender = userService.FindUserById(simpMessage.getSenderId()).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var receiver = userService.FindUserById(simpMessage.getReceiverId()).orElseThrow(() -> {
            throw new IllegalStateException("receiver does not exist");
        });

        ChatMessage newMessage = repository.save(new ChatMessage(sender, receiver, simpMessage.getMessage(), simpMessage.getMessageType()));
        sendMessageService.NotifyReceiver(receiver.getId(), sender.getId(), newMessage);

        Inbox senderInbox = new Inbox(sender, receiver, newMessage.getMessage(), newMessage.getMessageType(), true, true);
        Inbox receiverInbox = new Inbox(receiver, sender, newMessage.getMessage(), newMessage.getMessageType(), false, false);

        inboxService.AddInbox(senderInbox);
        inboxService.AddInbox(receiverInbox);

        return ChatMessageDto.FromChatMessage(newMessage);
    }

    @Transactional
    public UpdateResult UpdateSeen(long senderId, long messageId, boolean value) throws IllegalStateException {
        var sender = userService.FindUserById(senderId).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var message = repository.findById(messageId).orElseThrow(() -> {
            throw new IllegalStateException("message does not exist");
        });

        message.setSeen(value);
        sendMessageService.NotifySenderMessageReceived(senderId, messageId, value);

        return new UpdateResult(true);
    }

    @Transactional
    public UpdateResult UpdateSeenAll(long senderId, long receiverId, boolean value) throws IllegalStateException {
        var sender = userService.FindUserById(senderId).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var receiver = userService.FindUserById(receiverId).orElseThrow(() -> {
            throw new IllegalStateException("receiver does not exist");
        });

        var messages = repository.getChatMessageBySenderAndReceiverAndSeenIs(receiver, sender, false);
        for(var msg : messages) {
            msg.setSeen(value);
            sendMessageService.NotifySenderMessageReceived(senderId, msg.getId(), value);
        }

        return new UpdateResult(true);
    }

    public List<ChatMessageDto> getChatMessages(long senderId, long receiverId) throws IllegalStateException {
        var sender = userService.FindUserById(senderId).orElseThrow(() -> {
            throw new IllegalStateException("sender does not exist");
        });

        var receiver = userService.FindUserById(receiverId).orElseThrow(() -> {
            throw new IllegalStateException("receiver does not exist");
        });

        return repository
                .getChatMessages(sender, receiver)
                .stream()
                .map(ChatMessageDto::FromChatMessage)
                .collect(Collectors.toList());
    }
}
