package com.example.chatservice.message;

import com.example.chatservice.user.UserDto;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ChatMessageDto {
    static public ChatMessageDto FromChatMessage(ChatMessage message) {
        ChatMessageDto result = new ChatMessageDto();
        result.id = message.getId();
        result.sender = UserDto.FromChatUser(message.getSender());
        result.receiver = UserDto.FromChatUser(message.getReceiver());
        result.message = message.getMessage();
        result.messageType = ChatMessageType.valueOf(message.getMessageType());
        result.seen = message.isSeen();

        return result;
    }

    private long id;
    private UserDto sender;
    private UserDto receiver;
    private String message;
    private ChatMessageType messageType;
    private boolean seen;
}
