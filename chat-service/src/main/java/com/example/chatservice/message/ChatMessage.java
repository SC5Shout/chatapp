package com.example.chatservice.message;

import com.example.chatservice.user.ChatUser;
import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ChatMessage")
@Table(name = "ChatMessage")
public class ChatMessage {
    public ChatMessage(ChatUser sender, ChatUser receiver, String message, ChatMessageType messageType) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.messageType = messageType.getValue();
    }

    @Id
    @SequenceGenerator(name = "char_message_sequence", sequenceName = "char_message_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "char_message_sequence")
    @Column(name = "id", updatable = false)
    private long id;

    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "senderId", referencedColumnName = "id")
    private ChatUser sender;

    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "receiverId", referencedColumnName = "id")
    private ChatUser receiver;

    @Column(name = "message", nullable = false, columnDefinition = "TEXT")
    private String message;

    @Column(name = "messageType", nullable = false, columnDefinition = "TEXT")
    private String messageType;

    @Column(name = "seen", nullable = false, columnDefinition = "BOOL")
    private boolean seen;
}