package com.example.chatservice.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum ChatMessageType {
    TEXT("TEXT"),
    PNG("PNG");

    private final String value;
}
