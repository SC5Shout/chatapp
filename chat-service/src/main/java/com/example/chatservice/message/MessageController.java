package com.example.chatservice.message;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping(path = "message")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    //The frontend needs to get back the message
    //A socket callback on a client does not have a callback parameter
    //volley request does, that's why post instead of message
    //@MessageMapping("/chat")
    @PostMapping(path = "send")
    public ChatMessageDto SendMessage(@RequestBody SimpMessage stringMessage) throws IllegalStateException, JSONException {
        return messageService.AddMessage(stringMessage);
    }

    @PatchMapping(path = "seen/{senderId}/{messageId}/{value}")
    public UpdateResult UpdateSeen(@PathVariable long senderId, @PathVariable long messageId, @PathVariable boolean value) throws IllegalStateException {
        return messageService.UpdateSeen(senderId, messageId, value);
    }

    @PatchMapping(path = "all/seen/{senderId}/{receiverId}/{value}")
    public UpdateResult UpdateSeenAll(@PathVariable long senderId, @PathVariable long receiverId, @PathVariable boolean value) throws IllegalStateException {
        return messageService.UpdateSeenAll(senderId, receiverId, value);
    }

    @GetMapping(path = "bysenderandreceiver/{senderId}/{receiverId}")
    public List<ChatMessageDto> getMessagesBySenderAndReceiver(@PathVariable Long senderId, @PathVariable Long receiverId) throws IllegalStateException {
        return messageService.getChatMessages(senderId, receiverId);
    }
}
