package com.example.chatservice.message;

import com.example.chatservice.user.ChatUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<ChatMessage, Long> {
    @Query("SELECT cm from ChatMessage cm WHERE (cm.sender = ?1 AND cm.receiver = ?2) OR (cm.sender = ?2 AND cm.receiver = ?1) ORDER BY cm.id")
    List<ChatMessage> getChatMessages(ChatUser sender, ChatUser receiver);
    List<ChatMessage> getChatMessageBySenderAndReceiverAndSeenIs(ChatUser sender, ChatUser receiver, boolean seen);
}
