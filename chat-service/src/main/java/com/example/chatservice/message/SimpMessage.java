package com.example.chatservice.message;

import lombok.*;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SimpMessage {
    static public SimpMessage FromJson(JSONObject obj) throws JSONException {
        SimpMessage result = new SimpMessage();
        result.senderId = obj.getJSONObject("sender").getLong("id");
        result.receiverId = obj.getJSONObject("receiver").getLong("id");
        result.message = obj.getString("message");
        result.seen = obj.getBoolean("seen");
        result.messageType = ChatMessageType.valueOf(obj.getString("messageType"));
        return result;
    }

    private long senderId;
    private long receiverId;
    private String message;
    private ChatMessageType messageType;
    private boolean seen;
}
