package com.example.chatservice.message;

import lombok.AllArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SendMessageService {
    private final SimpMessagingTemplate messagingTemplate;

    void NotifyReceiver(long receiverId, long senderId, ChatMessage message) {
        String dest = receiverId + "/" + senderId;
        messagingTemplate.convertAndSend("/queue/receiver" + dest, message);
    }

    void NotifySenderMessageReceived(long senderId, long messageId, boolean value) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("messageId", messageId);
            obj.put("seen", value);

            String dest = "message.received/" + senderId;
            messagingTemplate.convertAndSend("/queue/" + dest, obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
