# Chat service and client applications for a university Java class.

Service was written using SpringBoot and a client application was written in Android Studio

The goal of this project was to learn about spring data JPA, REST and socket communication between client and server, Android Studio layouts and general java programming

### How to clone

`git clone https://gitlab.com/SC5Shout/chatapp.git`

### How to build/run

Configure your database, modify `chat-service/src/main/resources/application.properties` as you'd like.

Open chat-service in IntelliJ Idea, connect to your database and run the service

Open ChatApp in android studio and run multiple devices.

### TODO
- tests